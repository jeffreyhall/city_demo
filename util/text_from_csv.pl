# Normalized text data from a Spark workflow, to be used by an R workflow.

while (<>) {
  chomp;
  $_ =~ s/,/ /g;
  $_ =~ s/\s+/ /g;
  print "$_\n";
}