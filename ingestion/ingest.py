# Jeffrey Hall 2019

# import findspark
import yaml, os, sys
from typing import Any, Union
import couchdb

# findspark.init()

from pyspark import SparkContext, SparkConf
with open(os.path.dirname(os.path.abspath(os.path.dirname(sys.argv[0]))) + "/config/config.yaml", 'r') as infh:
    config = yaml.load(infh)

dataInbox = os.path.abspath(config["dataInbox"])
dataOutbox = os.path.abspath(config["dataOutbox"])
files_to_ingest = os.listdir(dataInbox)
delimiter = config["delimiter"]
couchUrl = config["couchUrl"]
database = config["couchDatabaseName"]

if 'processed' in files_to_ingest:
    files_to_ingest.pop(files_to_ingest.index('processed'))
else:
    os.mkdir(dataOutbox)

files = []
if files_to_ingest:
    for fname in files_to_ingest:  # type: Union[Union[str, unicode], Any]
        full_path = dataInbox + "/" + fname

        # If don't want ingestion jobs to span AWS instances, it's more
        # efficient to use the CouchDB .csv ingestor.  In production,
        # we'd need to add exception handling here for bad file formats,
        # killed processes, etc.

        # In production, we'd use a subprocess;  here I just run a system call. Either
        # way, output from the system call goes into the Spark log
        print "Ingesting file: " + full_path
        db_command = "cat " + full_path + "|couchimport --url " + couchUrl + " --db " + \
                        database + " --delimiter '" + delimiter + "'"
        os.system(db_command)
        os.rename(full_path, dataOutbox + "/" + fname)
        files.add(fname)

# In production, we would do some sanity checking here

couch = couchdb.Server(couchUrl)
db = couch[database]
nrows = db.view('_all_docs').total_rows

print str(nrows) + "ingested from files: " + ",".join(files)











