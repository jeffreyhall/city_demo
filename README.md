# README #

A skeleton workflow for an exploratory Data Science project

### Background ###

In this repo, we present a skeletal data workflow for an early ML project.
We assume, that early in a startup's development, they are looking at
ad-hoc data sets like:

   https://data.cityofnewyork.us/City-Government/Citywide-Payroll-Data-Fiscal-Year-/k397-673e 
   
and need a flexible protocol to ingest and begin the discovery of usable
business data from such data sets.  

The scripts in this repo, when made part of a Spark workflow, provide a
pipeline to:

    1) Automatically ingest new versions of this data set as they 
    become available;
    2) Automatically run two ML methods on each data ingestion; and
    3) Automatically generate QA plots for the ML methods and the
    data ingestion.
    
 In addition, the data scientists using the data could run these
 workflows on demand from PyCharm (or Jupyter.)
 
 
#### Limitations ####
 This skeleton provides the methods that would be used in the Spark 
 workflow, but doesn't implement the Spark plumbing;  each method
 is provided as separate R or Python script.  Also, no integration
 tests are provided, the unit tests weren't checked in, and there
 are gaps noted in the code between this skeleleton and production-
 ready code.
 
 In essence, this repo contains the sort of tools that a data scientist
 or data engineer would create in the first morning that he or she
 started working with this dataset.
 
### Workflows ###

The workflows provided are:

    * A data ingestion workflow.  Data is ingested into a typical
     NoSQL database
     
    * A text-mining workflow.  Topic modeling is performed on text
    from the data set.  This model is undirected and unsuperivised and
    is the first step for a general NLP workflow.  (Topic modeling is
    also an excellent method for the unsupervised validation of text 
    data *ingestion*, as we describe below.)
    
    * A boosted regression model, which predicts total wages from non-
    wage data.

The models demonstrate the use Python (scripted and in PySpark) and R,
using typical ML libraries (sklearn, Pandas/Numpy, and a few others.)

#### Ingestion ####

The ingestion workflow is a Python script to ingest sample data into 
a NoSQL database.  The database used was CouchDB, to demonstrate the
use of native database ingestors in a PySpark workflow.

In production, there would be a robust set of integration tests that 
are run with each new deployment.  Typically, unsupervised ML methods
like the two workflows described below would be run in Spark after 
each ingestion.

#### Text-mining ####

General NLP methods typically require domain-specific optimization
to be useful in real-life projects. (In fact, typically here is a
euphemism for "almost always".)  An excellent way to learn these
optimizations in a new project is the  method of "topic modeling."

Topic modeling (Pritchard et. al, 2000) learns words that are *associated*
with each other in a corpus of text, in an unsupervised and 
rigorous manner.  Topic modeling is well-known as the first step
in natural language text mining, but it has an additional benefit that
isn't as well-known:  it can identify missing data in rich natural language 
texts, and guide the imputation of missing data.  (Dr. Huopaniemi's paper below
is a very illustrative example.)  

Further pitfals that startups see in NLP projects
are described by Matt Honibal in his last PyData talk (https://www.youtube.com/watch?v=jpWqz85F_4Y).  

Forewarned is forearmed!  At least, in theory.

We should add that Topic Modeling is very useful when exploring data with text (for example, from kaggle.com, etc.) The
reason is that, when different sets of text are classified with a fixed number of topics, the **shape** of the histogram of
the topics doesn't change much between sets that have the same source.  This is a "quick-and-dirty"
way of validating data ingestion. Here's a typical example from a subset of the City Data data set:

![Topics](data/derived/topic_histogram.png)

Writing QA plots like this to a web dashboard after each ingestion is a good practice.  Tools like MapReduce or Spark
make these sorts of best-practices easy to automate.

#### Boosted regression ####

Finally, we present a simple ML workflow on the data itself.  Our model predicts gross wages

    Regular Gross Paid + Total OT Paid + Total Other Pay
    
from non-wage data for each employee in the data set.  The model chosen uses typical data
transformations needed to automate such models in a typical workflow.

![Predictions](data/derived/actual_vs_predicted.png)

The actual model used was an AdaBoost regression model.  This wouldn't be most Data Scientists' first choice
for such a model, but setting up this model in Python requires some additional manipulations
that might not be obvious at first glance:  see the file classifier.py for details. (Here, 
"might not be obvious" is a euphemism for "makes people swear the first time they try to do it".)


### Further reading ###
 1. Matplotlib User's Guide: https://matplotlib.org/users/index.html
 1. Pandas: A powerful Python data analysis toolkit (https://pandas.pydata.org/pandas-docs/stable/)
 1. Scikit Learn User Guide (https://scikit-learn.org/stable/user_guide.html)
 2. Topicmodels (R package) (https://cran.r-project.org/web/packages/topicmodels/topicmodels.pdf)
 3. Pritchard, J. K.; Stephens, M.; Donnelly, P. (June 2000). "Inference of population structure using multilocus genotype data". Genetics. 155 (2): 945–959.
 4. Freund, Yoav; Schapire, Robert E (1997). "A decision-theoretic generalization of on-line learning and an application to boosting". Journal of Computer and System Sciences. 55: 119.
 5. Huopaniemi, Ilkka et. al (2014) Disease progression subtype discovery from longitudinal EMR data with a majority of missing values and unknown initial time points. AMIA Annu Symp Proc. 2014; 2014: 709–718. (https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4419979/)

