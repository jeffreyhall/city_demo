# Jeffrey Hall, 2019

# Extract a random sample of lines from a text file, reserving a proportion for
# a validation set.  (In other words, samp[le without replacement for a test file,
# then sample without replacement from the sample.)

# If the requirement (say, for a bootstrap data science method) is to sample a large
# text file *with* replacement, or to sample an exact-size sample, see "Waterstone's
# Algorithm in Knuth. The Art of Computer Programming: Vol. 2, Seminumerical Algorithms.

import sys, random

fname = "text.csv"
ignore_first_line = True
sample_fname = "training.csv"
validationi_fname = "validation.csv"

proportion_of_file = .006
validation_sample_proportion = .20

with open(sample_fname, 'w') as outfh:
    with open(validationi_fname, 'w') as validation_outfh:
        with open(fname, 'r') as fh:

            sample_size = 0
            validation_size = 0
            population_size = 0

            if ignore_first_line:
                line = next(fh)

            for line in fh:
                population_size += 1
                if random.random() < proportion_of_file:
                    if random.random() < validation_sample_proportion:
                        validation_size += 1
                        validation_outfh.writelines([line])
                    else:
                        sample_size += 1
                        outfh.writelines([line])

print("Population size was " + str(population_size))
print("A sample set of size " + str(sample_size) + " was written to file " + str(sample_fname))
print("A validation set of size " + str(validation_size) + " was written to file " + str(validationi_fname))