# Jeffrey Hall, 2019

# Example of a classifier workflow for unstructured data
# This is a stand-alone script, but in production it would
# likely be a PySpark job

from sklearn.ensemble import AdaBoostRegressor
import yaml, os, sys
import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelBinarizer, LabelEncoder
import matplotlib.pyplot as plt


# Parameters are in the file .../conf/config.yaml:

with open(os.path.dirname(os.path.abspath(os.path.dirname(sys.argv[0]))) + "/config/config.yaml", 'r') as infh:
    config = yaml.load(infh)

dataInbox = os.path.abspath(config["dataInbox"])
dataOutbox = os.path.abspath(config["dataOutbox"])
infname = dataInbox + "/derived/training.csv"
validation_fname = dataInbox + "/derived/validation.csv"

# Hard-coding model parameters; in production, these would be passed to Spark job in the file config.yaml
n = 50
learning_rate=0.8

# The model
clf = AdaBoostRegressor(n_estimators=n, learning_rate=learning_rate)

# The data
with open(infname) as f:
    df_raw = pd.read_csv(f, header=None, parse_dates=True)

col_names = ['Fiscal Year', 'Payroll Number', 'Agency Name', 'Last Name', 'First Name', 'Mid Init', 'Agency Start Date',
             'Work Location Borough', 'Title Description', 'Leave Status as of June 30', 'Base Salary', 'Pay Basis',
             'Regular Hours', 'Regular Gross Paid', 'OT Hours', 'Total OT Paid', 'Total Other Pay']
df_raw.columns = col_names

# For this demo, we reject any data point with obviously bad dates.


# The model ordinates are all ingested data except for 'Payroll Number', 'Base Salary', 'Total OT Paid'
X = df_raw[['Fiscal Year', 'Agency Name', 'Last Name', 'First Name', 'Mid Init', 'Agency Start Date', 'Work Location Borough',
            'Title Description', 'Leave Status as of June 30', 'Pay Basis', 'Regular Hours', 'OT Hours']]
# The objective function is the total cost: Base Salary + Total OT Paid + Total Other Pay
Y = df_raw["Regular Gross Paid"] + df_raw["Total OT Paid"] + df_raw["Total Other Pay"]

# Encode text strings.  For this demo, ignoring the obvious sanity-checking transforms (None, NA, etc.)
encode = LabelEncoder()
str_cols = ['Agency Name', 'Last Name', 'First Name', 'Mid Init', 'Work Location Borough', 'Title Description',
            'Leave Status as of June 30', 'Pay Basis']
for col in str_cols:
    X[col] = encode.fit_transform(X[col])
    X[col] = pd.to_numeric(X[col], errors='coerce')

# Hard-coding the date parser;  this usually won't work for real messy data.
X['Agency Start Date'] =  pd.to_numeric( pd.to_datetime(X['Agency Start Date'], infer_datetime_format=True, errors='coerce') )

# Fit the model!
fit = clf.fit(X,Y)

# We validate against the validation set:

with open(validation_fname) as f:
    df_validation = pd.read_csv(f, header=None, parse_dates=True)
df_validation.columns = col_names
X_validation = df_validation[['Fiscal Year', 'Agency Name', 'Last Name', 'First Name', 'Mid Init', 'Agency Start Date',
                              'Work Location Borough', 'Title Description', 'Leave Status as of June 30', 'Pay Basis',
                              'Regular Hours', 'OT Hours']]
# The objective function is the total cost: Base Salary + Total OT Paid + Total Other Pay
Y_validation = df_validation["Regular Gross Paid"] + df_validation["Total OT Paid"] + df_validation["Total Other Pay"]
for col in str_cols:
    X_validation[col] = encode.fit_transform(X_validation[col])
    X_validation[col] = pd.to_numeric(X_validation[col], errors='coerce')

# Hard-coding the date parser;  this usually won't work for real messy data.
X_validation['Agency Start Date'] =  pd.to_numeric( pd.to_datetime(X_validation['Agency Start Date'],
                                                                   infer_datetime_format=True, errors='coerce') )

predictions = clf.predict(X_validation)
# plt.figure()
# plt.scatter(X, Y, c="k", label="Wagestraining set")
# plt.save(dataOutbox + "/training_set.png")
plt.figure()
plt.scatter(Y_validation, predictions,
            c="grey", marker="o", s=5, label="Wages vs. predicted wages")
dummy = np.linspace(min(predictions), max(predictions), max(predictions))
plt.plot(dummy,dummy, 'k-', c="r")
plt.title('Actual vs. predicted wages')
plt.savefig(dataOutbox + "/actual_vs_predicted.png")





